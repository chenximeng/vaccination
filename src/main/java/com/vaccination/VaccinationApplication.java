package com.vaccination;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.vaccination.mapper")
public class VaccinationApplication {

    public static void main(String[] args) {
        SpringApplication.run(VaccinationApplication.class, args);
    }

}
