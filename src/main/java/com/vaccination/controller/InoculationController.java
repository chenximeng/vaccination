package com.vaccination.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vaccination.entity.InoculationRecord;
import com.vaccination.entity.User;
import com.vaccination.service.InoculationRecordService;
import com.vaccination.service.UserService;
import com.vaccination.util.PageRequest;
import com.vaccination.util.PageResponse;
import com.vaccination.util.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@RestController
public class InoculationController {

    @Autowired
    private InoculationRecordService inoculationRecordService;

    @Autowired
    private UserService userService;

    @PostMapping("/listInoculations")
    public PageResponse listInoculations(HttpServletRequest request, PageRequest page) {
        String loginUser = (String) request.getSession().getAttribute("loginUser");
        User user = JSONObject.parseObject(loginUser, User.class);
        if (user == null) {
            PageResponse pageResponse = new PageResponse();
            pageResponse.setMsg("请登陆");
            return pageResponse;
        }
        if (user.getRole() == 2) {
            user.setId(-1L);
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        IPage<InoculationRecord> iPage = inoculationRecordService.listInoculations(new Page<>(page.getPage(), page.getLimit()), user.getId());
        List<InoculationRecord> records = iPage.getRecords();
        records.forEach(item -> {
            if (StringUtils.isBlank(item.getUsername()) && item.getUserId() != null) {
                User byId = userService.getById(item.getUserId());
                if (byId != null) {
                    item.setUsername(byId.getName());
                    item.setUserIdentity(byId.getIdentityNum());
                }
            }
            if (item.getInoculationTimeOne() != null) {
                item.setInoculationTimeStrOne(dateFormat.format(item.getInoculationTimeOne()));
            }
            if (item.getInoculationTimeTwo() != null) {
                item.setInoculationTimeStrTwo(dateFormat.format(item.getInoculationTimeTwo()));
            }
            if (item.getInoculationTimeThree() != null) {
                item.setInoculationTimeStrThree(dateFormat.format(item.getInoculationTimeThree()));
            }
        });
        return new PageResponse("0", "请求成功", iPage.getTotal(), records);
    }

    @GetMapping("/delInoculation")
    public Result delInoculation(Long id) {
        inoculationRecordService.removeById(id);
        return Result.success("删除成功");
    }

    @PostMapping("/saveInoculation")
    public Result saveInoculation(InoculationRecord record, HttpServletRequest request) throws ParseException {
        String loginUser = (String) request.getSession().getAttribute("loginUser");
        User user = JSONObject.parseObject(loginUser, User.class);
        if (user == null) {
            return Result.error("请登陆");
        }
        if(record.getStatusThree() == 1 && (record.getStatusTwo() == 2 || record.getStatusOne() == 2)) {
            return Result.error("请先接种第一二针");
        }

        if(record.getStatusTwo() == 1 && record.getStatusTwo() == 2) {
            return Result.error("请先接种第一针");
        }
        record.setUserId(user.getId());
        if (StringUtils.isNoneBlank(record.getUsername())){
            User byUsername = userService.getByUsername(record.getUsername());
            if(byUsername == null) {
                User newUser = new User();
                newUser.setUsername(record.getUsername());
                newUser.setName(record.getUsername());
                newUser.setPassword("123456");
                newUser.setRole(1);
                newUser.setStatus(1);
                userService.save(newUser);
                byUsername = newUser;
            }
            record.setUserId(byUsername.getId());
        }
        if (StringUtils.isNoneBlank(record.getInoculationTimeStrOne())) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            record.setInoculationTimeOne(dateFormat.parse(record.getInoculationTimeStrOne()));
        }
        if (StringUtils.isNoneBlank(record.getInoculationTimeStrTwo())) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            record.setInoculationTimeTwo(dateFormat.parse(record.getInoculationTimeStrTwo()));
        }
        if (StringUtils.isNoneBlank(record.getInoculationTimeStrThree())) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            record.setInoculationTimeThree(dateFormat.parse(record.getInoculationTimeStrThree()));
        }
        inoculationRecordService.save(record);
        return Result.success("添加成功");
    }

    @PostMapping("/updateInoculation")
    public Result updateInoculation(InoculationRecord record) throws ParseException {
        if (record.getId() == null) {
            return Result.error("更新失败");
        }

        if(record.getStatusThree() == 1 && (record.getStatusTwo() == 2 || record.getStatusOne() == 2)) {
            return Result.error("请先接种第一二针");
        }

        if(record.getStatusTwo() == 1 && record.getStatusTwo() == 2) {
            return Result.error("请先接种第一针");
        }

        if (StringUtils.isNoneBlank(record.getInoculationTimeStrOne())) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            record.setInoculationTimeOne(dateFormat.parse(record.getInoculationTimeStrOne()));
        }
        if (StringUtils.isNoneBlank(record.getInoculationTimeStrTwo())) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            record.setInoculationTimeTwo(dateFormat.parse(record.getInoculationTimeStrTwo()));
        }
        if (StringUtils.isNoneBlank(record.getInoculationTimeStrThree())) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            record.setInoculationTimeThree(dateFormat.parse(record.getInoculationTimeStrThree()));
        }
        inoculationRecordService.updateById(record);
        return Result.success("更新成功");
    }
}
