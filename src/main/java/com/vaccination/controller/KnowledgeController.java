package com.vaccination.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vaccination.entity.EpidemicPreventionKnowledge;
import com.vaccination.entity.User;
import com.vaccination.service.EpidemicPreventionKnowledgeService;
import com.vaccination.service.UserService;
import com.vaccination.util.PageRequest;
import com.vaccination.util.PageResponse;
import com.vaccination.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class KnowledgeController {

    @Autowired
    private EpidemicPreventionKnowledgeService epidemicPreventionKnowledgeService;
    
    @Autowired
    private UserService userService;

    @PostMapping("/listKnowledge")
    public PageResponse listKnowledge(HttpServletRequest request, PageRequest page) {
        String loginUser = (String) request.getSession().getAttribute("loginUser");
        User user = JSONObject.parseObject(loginUser, User.class);
        if (user == null) {
            PageResponse pageResponse = new PageResponse();
            pageResponse.setMsg("请登陆");
            return pageResponse;
        }
        if (user.getRole() == 2) {
            user.setId(-1L);
        }
        IPage<EpidemicPreventionKnowledge> iPage = epidemicPreventionKnowledgeService.listKnowledge(new Page<>(page.getPage(), page.getLimit()));
        List<EpidemicPreventionKnowledge> records = iPage.getRecords();
        records.forEach(item-> {
            if (item.getSendUserId() == null) {
                return;
            }
            User byId = userService.getById(item.getSendUserId());
            if (byId == null) {
                return;
            }
            item.setUsername(byId.getName());
        });
        return new PageResponse("0", "请求成功", iPage.getTotal(), records);
    }

    @GetMapping("/delKnowledge")
    public Result delCaseHistory(Long id, HttpServletRequest request) {
        String loginUser = (String) request.getSession().getAttribute("loginUser");
        User user = JSONObject.parseObject(loginUser, User.class);
        if (user == null) {
            return Result.error("请登陆");
        }
        if (user.getRole() == 1) {
            return Result.error("非管理员用户，删除失败");
        }
        epidemicPreventionKnowledgeService.removeById(id);
        return Result.success("删除成功");
    }

    @PostMapping("/saveKnowledge")
    public Result saveInoculation(EpidemicPreventionKnowledge record, HttpServletRequest request) {
        String loginUser = (String) request.getSession().getAttribute("loginUser");
        User user = JSONObject.parseObject(loginUser, User.class);
        if (user == null) {
            return Result.error("请登陆");
        }
        if (user.getRole() == 1) {
            return Result.error("非管理员用户，添加失败");
        }
        record.setSendUserId(user.getId());
        epidemicPreventionKnowledgeService.save(record);
        return Result.success("添加成功");
    }

    @PostMapping("/updateKnowledge")
    public Result updateInoculation(EpidemicPreventionKnowledge record, HttpServletRequest request) {
        String loginUser = (String) request.getSession().getAttribute("loginUser");
        User user = JSONObject.parseObject(loginUser, User.class);
        if (user == null) {
            return Result.error("请登陆");
        }
        if (user.getRole() == 1) {
            return Result.error("非管理员用户，修改失败");
        }
        if (record.getId() == null) {
            return Result.error("更新失败");
        }
        epidemicPreventionKnowledgeService.updateById(record);
        return Result.success("更新成功");
    }
}

