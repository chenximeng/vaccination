package com.vaccination.controller;

import com.alibaba.fastjson.JSONObject;
import com.sun.org.apache.xpath.internal.operations.Mod;
import com.vaccination.entity.EpidemicPreventionKnowledge;
import com.vaccination.entity.User;
import com.vaccination.service.EpidemicPreventionKnowledgeService;
import com.vaccination.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.time.OffsetDateTime;
import java.util.List;

@Controller
public class PageController {

    @Autowired
    EpidemicPreventionKnowledgeService epidemicPreventionKnowledgeService;
    @Autowired
    UserService userService;

    @GetMapping("/")
    public String index() {
        return "login";
    }

    @GetMapping("/toReg")
    public String toReg() {
        return "reg";
    }

    @GetMapping("/toInoculation")
    public ModelAndView toInoculation(ModelAndView mv, HttpServletRequest request) {
        mv.setViewName("yimiao");
        return getModelAndView(mv, request);
    }

    @GetMapping("/toCaseHistory")
    public ModelAndView toCaseHistory(ModelAndView mv, HttpServletRequest request) {
        mv.setViewName("case_history");
        return getModelAndView(mv, request);
    }

    @GetMapping("/toTestReport")
    public ModelAndView toTestReport(ModelAndView mv, HttpServletRequest request) {
        mv.setViewName("nucleic_test_report");
        return getModelAndView(mv, request);
    }

    @GetMapping("/toTravel")
    public ModelAndView toTravel(ModelAndView mv, HttpServletRequest request) {
        mv.setViewName("travel");
        return getModelAndView(mv, request);
    }

    @GetMapping("/toRiskArea")
    public ModelAndView toRiskArea(ModelAndView mv, HttpServletRequest request) {
        mv.setViewName("risk_area");
        return getModelAndView(mv, request);
    }

    @GetMapping("/toKnowledge")
    public ModelAndView toKnowledge(ModelAndView mv, HttpServletRequest request) {
        String loginUser = (String) request.getSession().getAttribute("loginUser");
        User user = JSONObject.parseObject(loginUser, User.class);
        if (user == null ){
            mv.addObject("msg", "请登陆");
            mv.setViewName("login");
            return mv;
        }
        if (user.getRole() == 2) {
            mv.setViewName("knowledge");
            mv.addObject("isAdmin", true);
            return mv;
        }
        List<EpidemicPreventionKnowledge> list = epidemicPreventionKnowledgeService.list();
        list.forEach(item -> {
            if (item.getSendUserId() == null) {
                return;
            }
            User byId = userService.getById(item.getSendUserId());
            if (byId == null) {
                return;
            }
            item.setUsername(byId.getName());
        });
        mv.addObject("knowledgeList", list);
        mv.setViewName("knowledge_user");
        return mv;
    }

    private ModelAndView getModelAndView(ModelAndView mv, HttpServletRequest request) {
        String loginUser = (String) request.getSession().getAttribute("loginUser");
        User user = JSONObject.parseObject(loginUser, User.class);
        if (user == null ){
            mv.addObject("msg", "请登陆");
            mv.setViewName("login");
            return mv;
        }
        if (user.getRole() == 2) {
            mv.addObject("isAdmin", true);
        }else {
            mv.addObject("isAdmin", false);
        }
        return mv;
    }

    @GetMapping("/toUserManage")
    public String toUserManage() {
        return "user_manage";
    }
    @GetMapping("/toEditInoculation")
    public String toEditInoculation() {
        return "editInoculation";
    }

    @GetMapping("/toEditCaseHistory")
    public String toEditCaseHistory() {
        return "editCaseHistory";
    }

    @GetMapping("/toEditTestReport")
    public String toEditTestReport() {
        return "editTestReport";
    }

    @GetMapping("/toEditTravel")
    public String toEditTravel() {
        return "editTravel";
    }

    @GetMapping("/toEditRiskArea")
    public String toEditRiskArea() {
        return "editRiskArea";
    }

    @GetMapping("/toEditKnowledge")
    public String editKnowledge() {
        return "editKnowledge";
    }
}
