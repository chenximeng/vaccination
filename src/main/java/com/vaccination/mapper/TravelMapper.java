package com.vaccination.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vaccination.entity.Travel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
* @author chenximeng
* @description 针对表【travel】的数据库操作Mapper
* @createDate 2022-01-13 12:13:09
* @Entity com.vaccination.entity.Travel
*/
public interface TravelMapper extends BaseMapper<Travel> {

    IPage<Travel> listTravel(Page<Travel> page, @Param("userId") Long userId);
}




