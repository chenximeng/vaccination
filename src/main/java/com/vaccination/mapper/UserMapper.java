package com.vaccination.mapper;

import com.vaccination.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @description 针对表【user】的数据库操作Mapper
* @createDate 2022-01-11 21:31:40
* @Entity com.vaccination.entity.User
*/
public interface UserMapper extends BaseMapper<User> {

}




