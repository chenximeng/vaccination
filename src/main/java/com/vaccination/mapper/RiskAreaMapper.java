package com.vaccination.mapper;

import com.vaccination.entity.RiskArea;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @description 针对表【risk_area】的数据库操作Mapper
* @createDate 2022-01-11 22:48:45
* @Entity com.vaccination.entity.RiskArea
*/
public interface RiskAreaMapper extends BaseMapper<RiskArea> {

}




