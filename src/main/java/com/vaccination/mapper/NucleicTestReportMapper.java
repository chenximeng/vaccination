package com.vaccination.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vaccination.entity.NucleicTestReport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
* @description 针对表【 nucleic_test_report】的数据库操作Mapper
* @createDate 2022-01-11 22:49:28
* @Entity com.vaccination.entity. nucleicTestReport
*/
public interface NucleicTestReportMapper extends BaseMapper<NucleicTestReport> {

    IPage<NucleicTestReport> listTestReport(Page<NucleicTestReport> page, @Param("userId") Long userId);
}




