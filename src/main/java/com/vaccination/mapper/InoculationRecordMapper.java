package com.vaccination.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vaccination.entity.InoculationRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
* @description 针对表【inoculation_record】的数据库操作Mapper
* @createDate 2022-01-11 22:49:13
* @Entity com.vaccination.entity.InoculationRecord
*/
public interface InoculationRecordMapper extends BaseMapper<InoculationRecord> {

    IPage<InoculationRecord> listInoculations(Page<InoculationRecord> page, @Param("userId") Long userId);
}




