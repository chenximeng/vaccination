package com.vaccination.mapper;

import com.vaccination.entity.EpidemicPreventionKnowledge;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @description 针对表【epidemic_prevention_knowledge】的数据库操作Mapper
* @createDate 2022-01-11 22:49:19
* @Entity com.vaccination.entity.EpidemicPreventionKnowledge
*/
public interface EpidemicPreventionKnowledgeMapper extends BaseMapper<EpidemicPreventionKnowledge> {

}




