package com.vaccination.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vaccination.entity.CaseHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vaccination.entity.InoculationRecord;
import org.apache.ibatis.annotations.Param;

/**
* @description 针对表【case_history】的数据库操作Mapper
* @createDate 2022-01-11 22:49:22
* @Entity com.vaccination.entity.CaseHistory
*/
public interface CaseHistoryMapper extends BaseMapper<CaseHistory> {

    IPage<CaseHistory> listCaseHistory(Page<CaseHistory> page, @Param("userId") Long userId);
}




