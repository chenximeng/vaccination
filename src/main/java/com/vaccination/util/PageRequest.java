package com.vaccination.util;

import lombok.Data;

@Data
public class PageRequest {

    private Integer page;

    private Integer limit;
}
