package com.vaccination.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 公共返回对象
 * @author cxm
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    private long status;
    private String msg;
    private Object data;

    /**
     * 成功返回结果
     *
     * @param msg
     * @param obj
     * @return
     */
    public static Result success(String msg, Object obj) {
        return new Result(200, msg, obj);
    }

    /**
     * 成功返回结果
     *
     * @param msg
     * @return
     */
    public static Result success(String msg) {
        return new Result(200, msg, null);
    }

    /**
     * 失败返回结果
     *
     * @param msg
     * @return
     */
    public static Result error(String msg) {
        return new Result(500, msg, null);
    }

    /**
     * 失败返回结果
     *
     * @param msg
     * @param obj
     * @return
     */
    public static Result error(String msg, Object obj) {
        return new Result(500, msg, obj);
    }

}
