package com.vaccination.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 防疫知识
 * @TableName epidemic_prevention_knowledge
 */
@TableName(value ="epidemic_prevention_knowledge")
@Data
public class EpidemicPreventionKnowledge implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 标题
     */
    @TableField(value = "title")
    private String title;

    /**
     * 内容
     */
    @TableField(value = "content")
    private String content;

    /**
     * 
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 发布人
     */
    @TableField(value = "send_user_id")
    private Long sendUserId;

    @TableField(exist = false)
    private String username;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}