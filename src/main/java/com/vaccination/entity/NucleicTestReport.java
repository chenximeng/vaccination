package com.vaccination.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName  nucleic_test_report
 */
@TableName(value ="nucleic_test_report")
@Data
public class NucleicTestReport implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 医院
     */
    @TableField(value = "hospital")
    private String hospital;

    /**
     * 检测时间
     */
    @TableField(value = "happen_time")
    private Date happenTime;

    /**
     * 结果
     */
    @TableField(value = "result")
    private String result;

    /**
     * 
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private Long userId;

    @TableField(exist = false)
    private String username;

    @TableField(exist = false)
    private String userIdentity;

    @TableField(exist = false)
    private String happenTimeStr;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}