package com.vaccination.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.sun.org.apache.bcel.internal.generic.IUSHR;
import lombok.Data;

/**
 * 
 * @TableName inoculation_record
 */
@TableName(value ="inoculation_record")
@Data
public class InoculationRecord implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 状态 1是2否
     */
    @TableField(value = "status_one")
    private Integer statusOne;

    /**
     * 状态 1是2否
     */
    @TableField(value = "status_two")
    private Integer statusTwo;


    /**
     * 状态 1是2否
     */
    @TableField(value = "status_three")
    private Integer statusThree;


    /**
     * 厂家
     */
    @TableField(value = "brand_one")
    private String brandOne;


    /**
     * 厂家
     */
    @TableField(value = "brand_two")
    private String brandTwo;

    /**
     * 厂家
     */
    @TableField(value = "brand_three")
    private String brandThree;

    /**
     * 接种时间
     */
    @TableField(value = "inoculation_time_one")
    private Date inoculationTimeOne;

    /**
     * 接种时间
     */
    @TableField(value = "inoculation_time_two")
    private Date inoculationTimeTwo;

    /**
     * 接种时间
     */
    @TableField(value = "inoculation_time_three")
    private Date inoculationTimeThree;

    /**
     * 
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private Long userId;

    @TableField(exist = false)
    private String username;

    @TableField(exist = false)
    private String userIdentity;

    @TableField(exist = false)
    private String inoculationTimeStrOne;

    @TableField(exist = false)
    private String inoculationTimeStrTwo;

    @TableField(exist = false)
    private String inoculationTimeStrThree;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}