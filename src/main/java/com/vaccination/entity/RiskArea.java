package com.vaccination.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName risk_area
 */
@TableName(value ="risk_area")
@Data
public class RiskArea implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 地名
     */
    @TableField(value = "name")
    private String name;

    /**
     * 级别
     */
    @TableField(value = "level")
    private Integer level;

    /**
     * 
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 发布人
     */
    @TableField(value = "send_user_id")
    private Long sendUserId;

    @TableField(exist = false)
    private String username;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}