package com.vaccination.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName user
 */
@TableName(value ="user")
@Data
public class User implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户名
     */
    @TableField(value = "username")
    private String username;

    /**
     * 密码
     */
    @TableField(value = "password")
    private String password;

    /**
     * 身份 1用户 2管理员
     */
    @TableField(value = "role")
    private Integer role;

    /**
     * 姓名
     */
    @TableField(value = "name")
    private String name;

    /**
     * 出生日期
     */
    @TableField(value = "birthday")
    private String birthday;

    /**
     * 电话号码
     */
    @TableField(value = "phone")
    private String phone;

    /**
     * 家庭住址
     */
    @TableField(value = "address")
    private String address;

    /**
     * 
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 状态1启用2禁用
     */
    @TableField(value = "status")
    private Integer status;

    /**
     * 身份证
     */
    @TableField(value = "identity_num")
    private String identityNum;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}