package com.vaccination.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 病例史
 * @TableName case_history
 */
@TableName(value ="case_history")
@Data
public class CaseHistory implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 病名
     */
    @TableField(value = "name")
    private String name;

    /**
     * 医院
     */
    @TableField(value = "hospital")
    private String hospital;

    /**
     * 发生时间
     */
    @TableField(value = "happen_time")
    private Date happenTime;

    @TableField(exist = false)
    private String happenTimeStr;

    /**
     * 
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private Long userId;

    @TableField(exist = false)
    private String username;

    @TableField(exist = false)
    private String userIdentity;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}