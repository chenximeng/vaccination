package com.vaccination.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName travel
 */
@TableName(value ="travel")
@Data
public class Travel implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 地名
     */
    @TableField(value = "name")
    private String name;

    /**
     * 出行时间
     */
    @TableField(value = "happen_time")
    private Date happenTime;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @TableField(value = "user_id")
    private Long userId;

    @TableField(exist = false)
    private String username;

    @TableField(exist = false)
    private String userIdentity;

    @TableField(exist = false)
    private String happenTimeStr;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}