package com.vaccination.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.vaccination.entity.User;
import com.vaccination.util.PageRequest;
import com.vaccination.util.PageResponse;

/**
* @description 针对表【user】的数据库操作Service
* @createDate 2022-01-11 21:31:40
*/
public interface UserService extends IService<User> {

    IPage<User> listPage(Page<User> userPage);

    User getByUsername(String username);

    void removeAll(Long userId);
}
