package com.vaccination.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vaccination.entity.RiskArea;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @description 针对表【risk_area】的数据库操作Service
* @createDate 2022-01-11 22:48:45
*/
public interface RiskAreaService extends IService<RiskArea> {

    IPage<RiskArea> listRiskArea(Page<RiskArea> page);
}
