package com.vaccination.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vaccination.entity.InoculationRecord;
import com.vaccination.service.InoculationRecordService;
import com.vaccination.mapper.InoculationRecordMapper;
import com.vaccination.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.soap.Addressing;
import java.util.List;

/**
* @description 针对表【inoculation_record】的数据库操作Service实现
* @createDate 2022-01-11 22:49:13
*/
@Service
public class InoculationRecordServiceImpl extends ServiceImpl<InoculationRecordMapper, InoculationRecord>
    implements InoculationRecordService{

    @Autowired
    private InoculationRecordMapper inoculationRecordMapper;

    @Override
    public IPage<InoculationRecord> listInoculations(Page<InoculationRecord> page, Long userId) {
        return inoculationRecordMapper.listInoculations(page, userId);
    }
}




