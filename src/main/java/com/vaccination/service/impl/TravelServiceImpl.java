package com.vaccination.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vaccination.entity.Travel;
import com.vaccination.service.TravelService;
import com.vaccination.mapper.TravelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author chenximeng
* @description 针对表【travel】的数据库操作Service实现
* @createDate 2022-01-13 12:13:09
*/
@Service
public class TravelServiceImpl extends ServiceImpl<TravelMapper, Travel>
    implements TravelService{

    @Autowired
    TravelMapper travelMapper;

    @Override
    public IPage<Travel> listTravel(Page<Travel> page, Long userId) {
        return travelMapper.listTravel(page, userId);
    }
}




