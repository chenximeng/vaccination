package com.vaccination.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vaccination.entity.*;
import com.vaccination.service.*;
import com.vaccination.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
* @description 针对表【user】的数据库操作Service实现
* @createDate 2022-01-11 21:31:40
*/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
    implements UserService{

    @Autowired
    UserMapper userMapper;
    @Autowired
    private CaseHistoryService caseHistoryService;
    @Autowired
    private EpidemicPreventionKnowledgeService epidemicPreventionKnowledgeService;
    @Autowired
    private InoculationRecordService inoculationRecordService;
    @Autowired
    private RiskAreaService riskAreaService;
    @Autowired
    private TravelService travelService;
    @Autowired
    private NucleicTestReportService nucleicTestReportService;

    @Override
    public IPage<User> listPage(Page<User> userPage) {
        return userMapper.selectPage(userPage, new QueryWrapper<>());
    }

    @Override
    public User getByUsername(String username) {
        return userMapper.selectOne(new QueryWrapper<User>().eq("username", username));
    }

    @Override
    @Transactional
    public void removeAll(Long userId) {
        caseHistoryService.remove(new QueryWrapper<CaseHistory>().eq("user_id", userId));
        epidemicPreventionKnowledgeService.remove(new QueryWrapper<EpidemicPreventionKnowledge>().eq("send_user_id", userId));
        inoculationRecordService.remove(new QueryWrapper<InoculationRecord>().eq("user_id", userId));
        riskAreaService.remove(new QueryWrapper<RiskArea>().eq("send_user_id", userId));
        travelService.remove(new QueryWrapper<Travel>().eq("user_id", userId));
        nucleicTestReportService.remove(new QueryWrapper<NucleicTestReport>().eq("user_id", userId));
        userMapper.deleteById(userId);
    }
}




