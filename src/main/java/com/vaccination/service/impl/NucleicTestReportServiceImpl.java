package com.vaccination.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vaccination.entity.NucleicTestReport;
import com.vaccination.mapper.NucleicTestReportMapper;
import com.vaccination.service.NucleicTestReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @description 针对表【 nucleic_test_report】的数据库操作Service实现
* @createDate 2022-01-11 22:49:28
*/
@Service
public class NucleicTestReportServiceImpl extends ServiceImpl<NucleicTestReportMapper, NucleicTestReport>
    implements NucleicTestReportService {

    @Autowired
    private NucleicTestReportMapper nucleicTestReportMapper;

    @Override
    public IPage<NucleicTestReport> listTestReport(Page<NucleicTestReport> page, Long userId) {
        return nucleicTestReportMapper.listTestReport(page, userId);
    }
}




