package com.vaccination.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vaccination.entity.RiskArea;
import com.vaccination.service.RiskAreaService;
import com.vaccination.mapper.RiskAreaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @description 针对表【risk_area】的数据库操作Service实现
* @createDate 2022-01-11 22:48:45
*/
@Service
public class RiskAreaServiceImpl extends ServiceImpl<RiskAreaMapper, RiskArea>
    implements RiskAreaService{

    @Autowired
    private RiskAreaMapper riskAreaMapper;

    @Override
    public IPage<RiskArea> listRiskArea(Page<RiskArea> page) {
        return riskAreaMapper.selectPage(page, new QueryWrapper<>());
    }
}




