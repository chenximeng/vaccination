package com.vaccination.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vaccination.entity.CaseHistory;
import com.vaccination.entity.InoculationRecord;
import com.vaccination.service.CaseHistoryService;
import com.vaccination.mapper.CaseHistoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @description 针对表【case_history】的数据库操作Service实现
* @createDate 2022-01-11 22:49:22
*/
@Service
public class CaseHistoryServiceImpl extends ServiceImpl<CaseHistoryMapper, CaseHistory>
    implements CaseHistoryService{

    @Autowired
    private CaseHistoryMapper caseHistoryMapper;

    @Override
    public IPage<CaseHistory> listCaseHistory(Page<CaseHistory> page, Long userId) {
        return caseHistoryMapper.listCaseHistory(page, userId);

    }
}




