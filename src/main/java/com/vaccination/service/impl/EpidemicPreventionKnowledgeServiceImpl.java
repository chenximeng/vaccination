package com.vaccination.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vaccination.entity.EpidemicPreventionKnowledge;
import com.vaccination.service.EpidemicPreventionKnowledgeService;
import com.vaccination.mapper.EpidemicPreventionKnowledgeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @description 针对表【epidemic_prevention_knowledge】的数据库操作Service实现
* @createDate 2022-01-11 22:49:19
*/
@Service
public class EpidemicPreventionKnowledgeServiceImpl extends ServiceImpl<EpidemicPreventionKnowledgeMapper, EpidemicPreventionKnowledge>
    implements EpidemicPreventionKnowledgeService{

    @Autowired
    private EpidemicPreventionKnowledgeMapper epidemicPreventionKnowledgeMapper;

    @Override
    public IPage<EpidemicPreventionKnowledge> listKnowledge(Page<EpidemicPreventionKnowledge> page) {
        return epidemicPreventionKnowledgeMapper.selectPage(page, new QueryWrapper<>());
    }
}




