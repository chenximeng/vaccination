package com.vaccination.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vaccination.entity.CaseHistory;
import com.vaccination.entity.NucleicTestReport;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @createDate 2022-01-11 22:49:28
*/
public interface NucleicTestReportService extends IService<NucleicTestReport> {
    IPage<NucleicTestReport> listTestReport(Page<NucleicTestReport> page, Long userId);
}
