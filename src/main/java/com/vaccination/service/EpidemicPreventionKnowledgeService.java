package com.vaccination.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vaccination.entity.EpidemicPreventionKnowledge;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @description 针对表【epidemic_prevention_knowledge】的数据库操作Service
* @createDate 2022-01-11 22:49:19
*/
public interface EpidemicPreventionKnowledgeService extends IService<EpidemicPreventionKnowledge> {

    IPage<EpidemicPreventionKnowledge> listKnowledge(Page<EpidemicPreventionKnowledge> page);
}
