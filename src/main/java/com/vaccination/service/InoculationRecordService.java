package com.vaccination.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vaccination.entity.InoculationRecord;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @description 针对表【inoculation_record】的数据库操作Service
* @createDate 2022-01-11 22:49:13
*/
public interface InoculationRecordService extends IService<InoculationRecord> {

    IPage<InoculationRecord> listInoculations(Page<InoculationRecord> page, Long userId);
}
