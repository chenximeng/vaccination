package com.vaccination.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vaccination.entity.NucleicTestReport;
import com.vaccination.entity.Travel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author chenximeng
* @description 针对表【travel】的数据库操作Service
* @createDate 2022-01-13 12:13:09
*/
public interface TravelService extends IService<Travel> {

    IPage<Travel> listTravel(Page<Travel> page, Long userId);
}
