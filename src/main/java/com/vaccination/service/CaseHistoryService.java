package com.vaccination.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vaccination.entity.CaseHistory;
import com.baomidou.mybatisplus.extension.service.IService;
import com.vaccination.entity.InoculationRecord;
import org.apache.ibatis.annotations.Param;

/**
* @description 针对表【case_history】的数据库操作Service
* @createDate 2022-01-11 22:49:22
*/
public interface CaseHistoryService extends IService<CaseHistory> {

    IPage<CaseHistory> listCaseHistory(Page<CaseHistory> page, Long userId);
}
