/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : vaccination

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 15/01/2022 22:35:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for case_history
-- ----------------------------
DROP TABLE IF EXISTS `case_history`;
CREATE TABLE `case_history` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(64) DEFAULT NULL COMMENT '病名',
  `hospital` varchar(64) DEFAULT NULL COMMENT '医院',
  `happen_time` datetime DEFAULT NULL COMMENT '发生时间',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `user_id` bigint DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for epidemic_prevention_knowledge
-- ----------------------------
DROP TABLE IF EXISTS `epidemic_prevention_knowledge`;
CREATE TABLE `epidemic_prevention_knowledge` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `send_user_id` bigint DEFAULT NULL COMMENT '发布人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for inoculation_record
-- ----------------------------
DROP TABLE IF EXISTS `inoculation_record`;
CREATE TABLE `inoculation_record` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `status_one` int DEFAULT NULL COMMENT '状态 1是2否',
  `status_two` int DEFAULT NULL,
  `status_three` int DEFAULT NULL,
  `brand_one` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '厂家',
  `brand_two` varchar(255) DEFAULT NULL,
  `brand_three` varchar(255) DEFAULT NULL,
  `inoculation_time_one` date DEFAULT NULL COMMENT '接种时间',
  `inoculation_time_two` date DEFAULT NULL,
  `inoculation_time_three` date DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `user_id` bigint DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for nucleic_test_report
-- ----------------------------
DROP TABLE IF EXISTS `nucleic_test_report`;
CREATE TABLE `nucleic_test_report` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `hospital` varchar(64) DEFAULT NULL COMMENT '医院',
  `happen_time` date DEFAULT NULL COMMENT '检测时间',
  `result` varchar(64) DEFAULT NULL COMMENT '结果',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `user_id` bigint DEFAULT NULL COMMENT '用户id',
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for risk_area
-- ----------------------------
DROP TABLE IF EXISTS `risk_area`;
CREATE TABLE `risk_area` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '地名',
  `level` int DEFAULT NULL COMMENT '级别 1低2中3高',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `send_user_id` bigint DEFAULT NULL COMMENT '发布人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for travel
-- ----------------------------
DROP TABLE IF EXISTS `travel`;
CREATE TABLE `travel` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(64) DEFAULT NULL COMMENT '地名',
  `happen_time` date DEFAULT NULL COMMENT '出行时间',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(64) DEFAULT NULL COMMENT '用户名',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `role` int DEFAULT NULL COMMENT '身份 1用户 2管理员',
  `name` varchar(64) DEFAULT NULL COMMENT '姓名',
  `birthday` varchar(64) DEFAULT NULL COMMENT '出生日期',
  `identity_num` varchar(64) DEFAULT NULL COMMENT '身份证',
  `phone` varchar(64) DEFAULT NULL COMMENT '电话号码',
  `address` varchar(255) DEFAULT NULL COMMENT '家庭住址',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `status` int DEFAULT '1' COMMENT '状态1启用2禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3;

SET FOREIGN_KEY_CHECKS = 1;
